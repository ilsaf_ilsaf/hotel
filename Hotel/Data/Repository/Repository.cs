﻿using Hotel.Data.Interface;
using Hotel.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Data.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {

        private readonly DBContext dBContext;
        private readonly DbSet<TEntity> dbSet;
        public Repository(DBContext dBContext)
        {
            this.dBContext = dBContext;
            dbSet = dBContext.Set<TEntity>();
        }

        public async Task AddAsync(TEntity item)
        {
            dbSet.Add(item);
            await dBContext.SaveChangesAsync();
        }

        public IQueryable<TEntity> GetAll()
        {
            return dbSet;
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await dbSet.FindAsync(id);
        }

        public async Task RemoveAsync(TEntity item)
        {
            dbSet.Remove(item);
            await dBContext.SaveChangesAsync();
        }

        public async Task SaveAsync()
        {
            await dBContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(TEntity item)
        {
            dbSet.Update(item);
            await dBContext.SaveChangesAsync();
        }
    }
}
