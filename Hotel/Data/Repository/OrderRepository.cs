﻿using Hotel.Data.Interface;
using Hotel.Entities;
using Hotel.Models.Order;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Data.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly DBContext context;

        public OrderRepository(DBContext context)
        {
            this.context = context;
        }

        public async Task AddOrderAsync(OrderViewModel model, bool editinfo, User user)
        {
            if (editinfo)
            {
                var neworder = new Order
                {
                    CheckInTime = model.CheckInTime,
                    CheckOutTime = model.CheckOutTime,
                    PhoneNumber = user.PhoneNumber,
                    FirstName = user.FirstName,
                    Email = user.Email,
                    FullPrice = model.FullPrice,
                    LastName = user.LastName,
                    Buyed = false,
                    Patronymic = user.LastName,
                    RoomId = model.RoomId,
                    OrderTime = DateTime.Now,
                    Statuses = Statuses.NotGiven
                };
                var neworderHistory = new OrderHistory
                {
                    CheckInTime = neworder.CheckInTime,
                    CheckOutTime = neworder.CheckOutTime,
                    FullPrice = neworder.FullPrice,
                    RoomId = neworder.RoomId,
                    OrderTime = DateTime.Now,
                    User = user,

                };
                await context.Orders.AddAsync(neworder);
                await context.OrderHistorys.AddAsync(neworderHistory);
            }
            else
            {
                var neworder = new Order
                {
                    CheckInTime = model.CheckInTime,
                    CheckOutTime = model.CheckOutTime,
                    PhoneNumber = model.PhoneNumber,
                    FirstName = model.FirstName,
                    Email = user.Email,
                    FullPrice = model.FullPrice,
                    LastName = model.LastName,
                    Buyed = false,
                    Patronymic = model.Patronymic,
                    RoomId = model.RoomId,
                    OrderTime = DateTime.Now,
                    Statuses = Statuses.NotGiven
                };
                var neworderHistory = new OrderHistory
                {
                    CheckInTime = neworder.CheckInTime,
                    CheckOutTime = neworder.CheckOutTime,
                    FullPrice = neworder.FullPrice,
                    RoomId = neworder.RoomId,
                    OrderTime = DateTime.Now,
                    User = user,
                };
                await context.Orders.AddAsync(neworder);
                await context.OrderHistorys.AddAsync(neworderHistory);
            }

            await context.SaveChangesAsync();
        }

        public async Task<bool> CheckOrderMyName(string Email)
        {
            var order = await context.Orders.Where(c => c.Email == Email).FirstOrDefaultAsync();

            if (order == null || order.OrderTime == DateTime.MinValue)
            {
                return false;
            }
            else return true;
        }

        public async Task ConfirmBuy(int id)
        {
            var order = await context.Orders.Where(x => x.Id == id).FirstOrDefaultAsync();

            order.Buyed = true;

            await context.SaveChangesAsync();
        }

        public async Task<Statuses> GetKeyStatusAsync(int id)
        {
            var keystatus = await context.Orders
                 .Where(x => x.Id == id)
                 .Select(x => x.Statuses)
                 .FirstOrDefaultAsync();

            return keystatus;
        }

        public async Task SwitchKeyAsync(int id)
        {
            var order = await context.Orders.Where(x => x.Id == id).FirstOrDefaultAsync();

            if (order.Statuses == Statuses.NotGiven)
            {
                order.Statuses = Statuses.Given;
            }
            else order.Statuses = Statuses.Completed;

            await context.SaveChangesAsync();
        }
    }
}
