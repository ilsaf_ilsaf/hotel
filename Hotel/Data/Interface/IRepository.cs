﻿using Hotel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Data.Interface
{
    public interface IRepository<TEntity>
    {
        public IQueryable<TEntity> GetAll();
        public Task<TEntity> GetByIdAsync(int id);
        public Task RemoveAsync(TEntity item);
        public Task AddAsync(TEntity item);
        public Task UpdateAsync(TEntity item);
        public Task SaveAsync();
    }
}
