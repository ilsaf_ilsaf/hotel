﻿using Hotel.Entities;
using Hotel.Models.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Data.Interface
{
    public interface IOrderRepository
    {
        public Task AddOrderAsync(OrderViewModel model, bool editinfo, User user);
        public Task<bool> CheckOrderMyName(string ClientName);
        public Task SwitchKeyAsync(int id);
        public Task ConfirmBuy(int id);
        public Task<Statuses> GetKeyStatusAsync(int id);
    }
}
