﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.CalendarServices
{
    public class CalendarService
    {
        private static Queue<DateTime> GetMonth(int month) 
        {
            int days = 0;

            DateTime firstMonday;
            DateTime lastSunday;

            DateTime firstDay = DateTime.Today.AddMonths(month).AddDays(-DateTime.Today.Day + 1);
            firstMonday = firstDay;

            DateTime lastDay = firstDay.AddDays(DateTime.DaysInMonth(firstDay.Year, firstDay.Month) - 1);
            lastSunday = lastDay;

            for (int x = 0; x < 6; x++)
            {
                if (firstMonday.DayOfWeek == DayOfWeek.Monday)
                {
                    break;
                }
                else
                {
                    firstMonday = firstMonday.AddDays(-1);
                    days++;
                }
            }

            for (int x = 0; x < 6; x++)
            {
                if (lastSunday.DayOfWeek == DayOfWeek.Sunday)
                {
                    break;
                }
                else
                {
                    lastSunday = lastSunday.AddDays(1);
                    days++;
                }
            }


            int calendarDays = DateTime.DaysInMonth(firstDay.Year, firstDay.Month) + days;

            Queue<DateTime> monthArray = new();

            for (int x = 0; x < calendarDays; x++)
            {
                monthArray.Enqueue(firstMonday);
                firstMonday = firstMonday.AddDays(1);
            }

            return monthArray;
        }


        public Queue<DateTime> GetFirstMonth() 
        {
            var firstMonth = GetMonth(0);
            return firstMonth;
        }

        public Queue<DateTime> GetSecondMonth()
        {
            var secondtMonth = GetMonth(1);
            return secondtMonth;
        }
        

        public int WeeksCountFirstMonth()
        {
            return GetFirstMonth().Count / 7;
        }
        public int WeeksCountSecondMonth()
        {
            return GetSecondMonth().Count / 7;
        }
    }
}
