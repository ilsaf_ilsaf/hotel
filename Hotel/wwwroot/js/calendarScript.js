function select() {
    var result = []; 
    document.querySelectorAll('.date_selected').forEach(item => {
        result.push([item.dataset.date]);
    });

    document.getElementById("InTime").value = result[0];
    document.getElementById("OutTime").value = result[1];

    if (result.length == 2) {
        var tableSelecteds = Array.from(document.querySelectorAll('.date_enabled'))
            .filter(el => ConvertToDate(el.dataset.date) > ConvertToDate(result[0]) && ConvertToDate(el.dataset.date) < ConvertToDate(result[1]));

        tableSelecteds.forEach(item => {
            item.classList.toggle("date_selected");
        });
    }

    console.log(tableSelecteds);
}

function ConvertToDate(el) {
    var parts = el.toString().split(' ');
    var normalDate = parts[0].split('.');

    return new Date(normalDate[2], normalDate[1] - 1, normalDate[0]);
}

document.querySelectorAll('.js-table-select .date_enabled').forEach(item => {
    item.addEventListener('click', event => {
        if (document.querySelectorAll('.js-table-select .date_selected').length < 2 || item.classList.toggle("date_selected")) {
        }
        else {
            document.querySelectorAll('.date_selected').forEach(item => {
                item.classList.toggle("date_selected");
            });
            return;

        }

        if (item.classList.toggle("date_selected")) {
            select();
        }
    })
})