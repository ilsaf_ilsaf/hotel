﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Entities
{
    public class Employee
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; } 
        public DateTime Birhrday { get; set; }
        public string Phone { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }
    }
}
