﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Entities
{
    public class RoomCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public uint PlacesQuantity { get; set; }
        public uint RoomQuantity { get; set; }
        public string Image { get; set; }
        public uint Price { get; set; }
        public string Description { get; set; } 
        public ICollection<Room> Rooms { get; set; }
        public RoomCategory()
        {
            Rooms = new List<Room>();
        }
    }
}
