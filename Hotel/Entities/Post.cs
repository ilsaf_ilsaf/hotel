﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Entities
{
    public class Post
    {
        public int Id { get; set; }
        public string PostName { get; set; }
        public uint Salary { get; set; }
        public ICollection<Employee> Employees { get; set; }
        public Post()
        {
            Employees = new List<Employee>();
        }
    }
}
