﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Entities
{
    public class OrderHistory
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public int FullPrice { get; set; }
        public DateTime OrderTime { get; set; }
        public DateTime CheckInTime { get; set; }
        public DateTime CheckOutTime { get; set; }
        public Room Room { get; set; }
        public User User { get; set; }
        public Statuses Statuses { get; set; }
    }
}
