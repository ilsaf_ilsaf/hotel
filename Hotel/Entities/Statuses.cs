﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Entities
{
    public enum Statuses
    {
        NotGiven,
        Given,
        Completed,
        Expired
    }
}
