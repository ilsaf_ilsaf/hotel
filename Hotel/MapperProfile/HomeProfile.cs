﻿using Hotel.Entities;
using Hotel.Models.Home;
using AutoMapper;

namespace Hotel.MapperProfile
{
    public class HomeProfile : Profile
    {
        public HomeProfile()
        {
            CreateMap<RoomCategory, HomeListViewModel>();

            CreateMap<Room, RoomListViewModel>();
        }
    }
}
