﻿using AutoMapper;
using Hotel.Entities;
using Hotel.Models.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.MapperProfile
{
    public class OrderProfile :Profile
    {
        public OrderProfile()
        {
            CreateMap<OrderHistory, OrderHistoryViewModel>();
            CreateMap<Order, AllOrdersViewModel>();
        }
    }
}
