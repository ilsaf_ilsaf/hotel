﻿using AutoMapper;
using Hotel.Entities;
using Hotel.Models.Reception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.MapperProfile
{
    public class ReceptionProfile : Profile
    {
        public ReceptionProfile()
        {
            CreateMap<Employee, EmployeeListViewModel>();

            CreateMap<Employee, AddEmployeeViewModel>();

            CreateMap<OrderHistory, AllOrdersHistoryViewModel>();
        }
    }
}
