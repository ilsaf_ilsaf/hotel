﻿using Hotel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models.Home
{
    public class RoomListViewModel
    {
        public int Id { get; set; }
        public uint Floor { get; set; }
        public RoomCategory RoomCategory { get; set; }
    }
}
