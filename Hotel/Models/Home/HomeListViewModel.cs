﻿using Hotel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models.Home
{
    public class HomeListViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public uint PlacesQuantity { get; set; }
        public uint RoomQuantity { get; set; }
        public string Image { get; set; }
        public uint Price { get; set; }
        public string Description { get; set; }
        public List<RoomCategoryImages> RoomImages { get; set; }
    }
}
