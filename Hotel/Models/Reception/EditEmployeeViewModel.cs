﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models.Reception
{
    public class EditEmployeeViewModel
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public DateTime Birhrday { get; set; }
        public string Phone { get; set; }
        public int PostId { get; set; }
    }
}
