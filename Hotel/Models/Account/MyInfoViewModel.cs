﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models.Account
{
    public class MyInfoViewModel
    {
        public string UserName { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public DateTime Birhrday { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}
