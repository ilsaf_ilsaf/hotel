﻿using Hotel.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models.Order
{
    public class OrderViewModel
    {
        public string Id { get; set; }

        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Отчество")]
        public string Patronymic { get; set; }

        [Display(Name = "Номер телефона")]
        public string PhoneNumber { get; set; }
        public int FullPrice { get; set; }
        public int RoomId { get; set; }
        public DateTime CheckInTime { get; set; }
        public DateTime CheckOutTime { get; set; }

        [Display(Name = "Дни")]
        public int Days { get; set; }

        public Statuses Statuses = Statuses.NotGiven;
    }
}
