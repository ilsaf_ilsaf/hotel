﻿using Hotel.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models.Order
{
    public class OrderHistoryViewModel
    {
        public int Id { get; set; }
        public int FullPrice { get; set; }
        public DateTime OrderTime { get; set; }
        public DateTime CheckInTime { get; set; }
        public DateTime CheckOutTime { get; set; }
        public Room Room { get; set; }

    }
}
