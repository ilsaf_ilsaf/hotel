﻿using Hotel.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Models.Order
{
    public class ReservationInfoViewModel
    {
        public DateTime InTime { get; set; }
        public DateTime OutTime { get; set; }
        public int RoomId { get; set; }
        public Room Room { get; set; }
        public Queue<DateTime> FirstMonth { get; set; }
        public Queue<DateTime> SecondtMonth { get; set; }
        public List<DateTime> AllOlders { get; set; }
    }
}
