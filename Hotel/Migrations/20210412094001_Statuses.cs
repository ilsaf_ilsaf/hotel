﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Hotel.Migrations
{
    public partial class Statuses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderHistorys_OrderStatuss_OrderStatusId",
                table: "OrderHistorys");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_OrderStatuss_OrderStatusId",
                table: "Orders");

            migrationBuilder.DropTable(
                name: "OrderStatuss");

            migrationBuilder.DropIndex(
                name: "IX_Orders_OrderStatusId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_OrderHistorys_OrderStatusId",
                table: "OrderHistorys");

            migrationBuilder.DropColumn(
                name: "OrderStatusId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "OrderStatusId",
                table: "OrderHistorys");

            migrationBuilder.AddColumn<int>(
                name: "Statuses",
                table: "Orders",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Statuses",
                table: "OrderHistorys",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Statuses",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "Statuses",
                table: "OrderHistorys");

            migrationBuilder.AddColumn<int>(
                name: "OrderStatusId",
                table: "Orders",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OrderStatusId",
                table: "OrderHistorys",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "OrderStatuss",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoomId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderStatuss", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderStatuss_Rooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "Rooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Orders_OrderStatusId",
                table: "Orders",
                column: "OrderStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderHistorys_OrderStatusId",
                table: "OrderHistorys",
                column: "OrderStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderStatuss_RoomId",
                table: "OrderStatuss",
                column: "RoomId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderHistorys_OrderStatuss_OrderStatusId",
                table: "OrderHistorys",
                column: "OrderStatusId",
                principalTable: "OrderStatuss",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_OrderStatuss_OrderStatusId",
                table: "Orders",
                column: "OrderStatusId",
                principalTable: "OrderStatuss",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
