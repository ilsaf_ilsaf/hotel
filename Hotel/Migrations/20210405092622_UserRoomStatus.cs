﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Hotel.Migrations
{
    public partial class UserRoomStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "RoomStatuss",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RoomStatuss_UserId",
                table: "RoomStatuss",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_RoomStatuss_AspNetUsers_UserId",
                table: "RoomStatuss",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoomStatuss_AspNetUsers_UserId",
                table: "RoomStatuss");

            migrationBuilder.DropIndex(
                name: "IX_RoomStatuss_UserId",
                table: "RoomStatuss");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "RoomStatuss");
        }
    }
}
