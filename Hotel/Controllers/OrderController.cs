﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Hotel.CalendarServices;
using Hotel.Data.Interface;
using Hotel.EmailServices;
using Hotel.Entities;
using Hotel.Models.Order;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Controllers
{
    public class OrderController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IOrderRepository _repositoryOrder;
        private readonly IRepository<Room> _roomRepository;
        private readonly IRepository<OrderHistory> _historyRepository;
        private readonly IRepository<Order> _repository;
        private readonly IMapper mapper;
        private readonly CalendarService _calendar;

        public OrderController(IOrderRepository repositoryOrder, UserManager<User> userManager, IRepository<Room> roomRepository, IRepository<OrderHistory> historyRepository,
            IMapper mapper, IRepository<Order> repository, CalendarService calendar)
        {
            _repositoryOrder = repositoryOrder;
            _userManager = userManager;
            _roomRepository = roomRepository;
            _historyRepository = historyRepository;
            this.mapper = mapper;
            _repository = repository;
            _calendar = calendar;
        }

        public async Task<IActionResult> ReservationInfo(int roomId)
        {
            if (!User.Identity.IsAuthenticated)
            {
                TempData["alert"] = "Необходимо войти в учетную запись или зарегистрироваться!";
                return RedirectToAction("AllRooms", "Home");
            }

            var user = await _userManager.FindByNameAsync(HttpContext.User.Identity.Name);

            bool check = await _repositoryOrder.CheckOrderMyName(user.Email);

            if (!user.EmailConfirmed)
            {
                TempData["alert"] = "Email должна быть подтверждена!";
                return RedirectToAction("AllRooms", "Home");
            }
            else if (check)
            {
                TempData["alert"] = "Клиент может забронировать только один номер!";
                return RedirectToAction("AllRooms", "Home");

            }

            var room = await _roomRepository.GetAll()
            .Include(c => c.RoomCategory)
            .Where(x => x.Id == roomId)
            .FirstOrDefaultAsync();

            var allorders = await _repository.GetAll().Where(q => q.RoomId == roomId).ToListAsync();

            List<DateTime> rooms = new();
            foreach (var roomdays in allorders)
            {
                if (roomdays.OrderTime != DateTime.MinValue)
                {
                    for (var day = roomdays.CheckInTime; day.Date <= roomdays.CheckOutTime; day = day.AddDays(1))
                    {
                        rooms.Add(day.Date);
                    }
                }
            }

            var firstMonth = _calendar.GetFirstMonth();
            var secondtMonth = _calendar.GetSecondMonth();

            var rmodel = new ReservationInfoViewModel
            {
                Room = room,
                RoomId = roomId,
                FirstMonth = firstMonth,
                SecondtMonth = secondtMonth,
                AllOlders = rooms
            };

            ViewData["NameMonth"] = DateTime.Now.ToMonthName().ToUpper();
            ViewData["NameNextMonth"] = DateTime.Now.AddMonths(1).ToMonthName().ToUpper();
            ViewData["WeeksContFirstMonth"] = _calendar.WeeksCountFirstMonth();
            ViewData["WeeksContSecondMonth"] = _calendar.WeeksCountSecondMonth();

            return View(rmodel);

        }

        [HttpPost]
        public async Task<IActionResult> Reservation(ReservationInfoViewModel model, string returnUrl)
        {
            var user = await _userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var time = model.OutTime - model.InTime;

            var room = await _roomRepository.GetAll()
                .Include(c => c.RoomCategory)
                .Where(x => x.Id == model.RoomId)
                .FirstOrDefaultAsync();

            var allolders = await _repository.GetAll().Where(q => q.RoomId == model.RoomId).ToListAsync();

            foreach (var roomdays in allolders)
            {
                for (var day = roomdays.CheckInTime; day.Date <= roomdays.CheckOutTime; day = day.AddDays(1))
                {
                    for (var realday = model.InTime; realday.Date <= model.OutTime; realday = realday.AddDays(1))
                    {
                        if (roomdays.OrderTime != DateTime.MinValue)
                        {
                            if (day.Date == realday.Date)
                            {
                                TempData["alert"] = "Комната не доступна в данный период, так как пересекаются с другими занятыми днями!";
                                return Redirect(returnUrl);
                            }
                        }
                    }
                }
            }

            int price = (int)(Convert.ToInt32(time.Days + 1) * room.RoomCategory.Price);

            var modelr = new OrderViewModel
            {
                RoomId = room.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Patronymic = user.Patronymic,
                PhoneNumber = user.PhoneNumber,
                Days = time.Days + 1,
                FullPrice = price,
                CheckInTime = model.InTime,
                CheckOutTime = model.OutTime
            };
            return View(modelr);
        }

        [HttpPost]
        public async Task<IActionResult> CompleteReservation(OrderViewModel model, bool editinfo = false)
        {
            var user = await _userManager.FindByNameAsync(HttpContext.User.Identity.Name);

            await _repositoryOrder.AddOrderAsync(model, editinfo, user);

            var room = await _roomRepository.GetAll()
                .Include(k => k.RoomCategory)
                .Where(x => x.Id == model.RoomId)
                .FirstOrDefaultAsync();

            EmailService emailService = new EmailService();
            string message = $"Спасибо за заказ!" +
                $"Тип комнаты => {room.RoomCategory.Name}.\nЭтаж: {room.Floor}.\nЦена:{model.FullPrice} " +
                $"на {model.Days} сут. \nЗаезд:{model.CheckInTime.Date.ToString("dd/MM/yyyy")}. \nВыезд:{model.CheckOutTime.Date.ToString("dd/MM/yyyy")}. \nФамилия:{model.LastName}." +
                $"Имя:{model.FirstName}. Отчество:{model.Patronymic}.\nТелефон для связи:+74584798742. Адрес:г.Набережные Челны, проспект Сююмбике, д.2.\n Ждём вас снова!!!";
            await emailService.SendEmailAsync(user.Email, "Информация о заказе", message);

            return RedirectToAction("OrderHistoryMyName");
        }

        public async Task<IActionResult> OrderHistoryMyName()
        {
            var user = await _userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var orderHistory = await _historyRepository.GetAll()
                .Include(x => x.User)
                .Include(x => x.Room.RoomCategory)
                .Include(x => x.Room)
                .Where(x => x.User.Id == user.Id)
                .OrderByDescending(x => x.CheckInTime)
                .ProjectTo<OrderHistoryViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();

            var actualroom = await _repository.GetAll().Include(x => x.Room.RoomCategory).Where(m => m.Email == user.Email && m.OrderTime != DateTime.MinValue).FirstOrDefaultAsync();

            if (actualroom != null)
            {
                var days = actualroom.CheckOutTime - actualroom.CheckInTime;
                ViewData["Name"] = actualroom.Room.RoomCategory.Name;
                ViewData["CheckInTime"] = actualroom.CheckInTime.ToString("dd/MM/yyyy");
                ViewData["CheckOutTime"] = actualroom.CheckOutTime.ToString("dd/MM/yyyy");
                ViewData["FullPrice"] = actualroom.FullPrice;
                ViewData["Days"] = days.Days + 1;
                ViewData["OrderId"] = actualroom.Id;
            }
            else
            {
                ViewData["Name"] = null;
            }

            return View(orderHistory);
        }


        public async Task<IActionResult> CancelMyOrder(int orderId)
        {
            var order = await _repository.GetByIdAsync(orderId);
            await _repository.RemoveAsync(order);

            return RedirectToAction("OrderHistoryMyName");
        }
    }
}
