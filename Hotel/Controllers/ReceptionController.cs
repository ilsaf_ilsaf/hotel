﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Hotel.Data.Interface;
using Hotel.Entities;
using Hotel.Models.Order;
using Hotel.Models.Reception;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Controllers
{
    [Authorize(Roles = "reception")]
    public class ReceptionController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IRepository<Employee> _repositoryemployee;
        private readonly IRepository<Post> _repositoryPost;
        private readonly IRepository<OrderHistory> _historyRepository;
        private readonly IRepository<Order> _orderRepository;
        private readonly IOrderRepository _repository;
        private readonly IMapper mapper;
        public ReceptionController(IRepository<Employee> repositoryemployee,
            IRepository<Post> repositoryPost, IMapper mapper, UserManager<User> userManager,
            IRepository<OrderHistory> historyRepository, IRepository<Order> orderRepository, IOrderRepository repository)
        {
            _repositoryemployee = repositoryemployee;
            _repositoryPost = repositoryPost;
            this.mapper = mapper;
            _userManager = userManager;
            _historyRepository = historyRepository;
            _orderRepository = orderRepository;
            _repository = repository;
        }

        public async Task<IActionResult> AllEmployees()
        {
            var allEmployee = await _repositoryemployee.GetAll()
                .Include(x => x.Post)
                .ProjectTo<EmployeeListViewModel>(mapper.ConfigurationProvider)
                .OrderBy(x => x.Id)
                .ToListAsync();

            return View(allEmployee);
        }

        public async Task<IActionResult> AddEmployee()
        {
            var allpost = await _repositoryPost.GetAll().ToListAsync();
            ViewBag.Post = new SelectList(allpost, "Id", "PostName");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddEmployee(AddEmployeeViewModel model)
        {
            var employee = new Employee
            {
                LastName = model.LastName,
                FirstName = model.FirstName,
                Patronymic = model.Patronymic,
                Birhrday = model.Birhrday,
                Phone = model.Phone,
                PostId = model.PostId
            };

            await _repositoryemployee.AddAsync(employee);

            return RedirectToAction("AllEmployees");
        }

        public async Task<IActionResult> DeleteEmployee(int id)
        {
            var employee = await _repositoryemployee.GetByIdAsync(id);
            await _repositoryemployee.RemoveAsync(employee);

            return RedirectToAction("AllEmployees");
        }

        public async Task<IActionResult> EditEmployee(int id)
        {
            ViewBag.Post = new SelectList(await _repositoryPost.GetAll().ToListAsync(), "Id", "PostName");

            var employee = await _repositoryemployee.GetByIdAsync(id);
            var modelemployee = new EditEmployeeViewModel
            {
                Id = employee.Id,
                Birhrday = employee.Birhrday,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Patronymic = employee.Patronymic,
                Phone = employee.Phone,
                PostId = employee.PostId
            };

            return View(modelemployee);
        }

        [HttpPost]
        public async Task<IActionResult> EditEmployee(EditEmployeeViewModel model)
        {
            var employee = await _repositoryemployee.GetAll()
                .Where(x => x.Id == model.Id)
                .FirstOrDefaultAsync();
            if (employee != null)
            {
                employee.LastName = model.LastName;
                employee.FirstName = model.FirstName;
                employee.Patronymic = model.Patronymic;
                employee.Birhrday = model.Birhrday;
                employee.Phone = model.Phone;
                employee.PostId = model.PostId;
                await _repositoryemployee.SaveAsync();
            }

            await _repositoryemployee.UpdateAsync(employee);

            return RedirectToAction("AllEmployees");
        }

        public async Task<IActionResult> DeleteSelectedEmployeers(List<int> employersId)
        {
            foreach (var id in employersId)
            {
                var employee = await _repositoryemployee.GetByIdAsync(id);
                if (employee != null)
                {
                    await _repositoryemployee.RemoveAsync(employee);
                }
            }
            return RedirectToAction("AllEmployees");
        }

        public async Task<IActionResult> AllOrdersHistory(string sortorders, string search, string personalInfo, DateTime dateTime)
        {
            var order = _historyRepository.GetAll();

            switch (sortorders)
            {
                case "checkInUp": order = order.OrderBy(x => x.CheckInTime); break;
                case "checkOutUp": order = order.OrderBy(x => x.CheckOutTime); break;
                case "checkInDown": order = order.OrderByDescending(x => x.CheckInTime); break;
                case "checkOutDown": order = order.OrderByDescending(x => x.CheckOutTime); break;
            }

            switch (search)
            {
                case "searchIn": order = order.Where(x => x.CheckInTime == dateTime); break;
                case "searchOut": order = order.Where(y => y.CheckOutTime == dateTime); break;
                case "searchPrice": order = order.Where(z => z.FullPrice == Convert.ToInt32(personalInfo)); break;
            }

            if (search != null)
            {
                var allOrdersUser = order
                 .Include(x => x.User);

                switch (search)
                {
                    case "searchLastName": order = allOrdersUser.Where(x => x.User.LastName == personalInfo); break;
                    case "searchName": order = allOrdersUser.Where(x => x.User.FirstName == personalInfo); break;
                    case "searchPatr": order = allOrdersUser.Where(x => x.User.Patronymic == personalInfo); break;
                }
            }

            var allOrders = await order
                .Include(x => x.User)
                .Include(x => x.Room.RoomCategory)
            .ProjectTo<AllOrdersHistoryViewModel>(mapper.ConfigurationProvider)
            .ToListAsync();

            return View(allOrders);
        }

        public async Task<IActionResult> AllOrders(string sortorders, string search, string info, DateTime dateTime, string keygive)
        {
            var order = _orderRepository.GetAll();

            switch (sortorders)
            {
                case "checkInUp": order = order.OrderBy(x => x.CheckInTime); break;
                case "checkOutUp": order = order.OrderBy(x => x.CheckOutTime); break;
                case "checkInDown": order = order.OrderByDescending(x => x.CheckInTime); break;
                case "checkOutDown": order = order.OrderByDescending(x => x.CheckOutTime); break;
            }

            switch (search)
            {
                case "searchIn": order = order.Where(x => x.CheckInTime == dateTime); break;
                case "searchOut": order = order.Where(y => y.CheckOutTime == dateTime); break;
                case "searchPrice": order = order.Where(z => z.FullPrice == Convert.ToInt32(info)); break;
                case "searchLastName": order = order.Where(x => x.LastName == info); break;
                case "searchName": order = order.Where(x => x.FirstName == info); break;
                case "searchPatr": order = order.Where(x => x.Patronymic == info); break;
                case "searchPhone": order = order.Where(x => x.PhoneNumber == info); break;
                case "searchMail": order = order.Where(x => x.Email == info); break;
            }

            if (search == "searchStatus")
            {
                var statusroom = order.Include(k => k.Room).Include(x => x.Room.RoomCategory);
                switch (keygive)
                {
                    case "give": order = statusroom.Where(k => k.Statuses == Statuses.Given); break;
                    case "notgive": order = statusroom.Where(k => k.Statuses == Statuses.NotGiven); break;
                }
            }

            if(search == "searchRoom")
            {
                var roomname = order.Include(x => x.Room.RoomCategory).Include(k => k.Room);

                order = roomname.Where(f=> f.Room.RoomCategory.Name == info);
            }

            var allorders = await order
                   .Include(x => x.Room.RoomCategory)
                   .ProjectTo<AllOrdersViewModel>(mapper.ConfigurationProvider)
                   .ToListAsync();

            return View(allorders);
        }

        public async Task<IActionResult> Key(int id)
        {
            await _repository.SwitchKeyAsync(id);

            return RedirectToAction("AllOrders");
        }

        public async Task<IActionResult> Buyed(int id)
        {
            await _repository.ConfirmBuy(id);

            return RedirectToAction("AllOrders");
        }

        public async Task<IActionResult> DeleteOrder(int orderId)
        {
            var keystatus = await _repository.GetKeyStatusAsync(orderId);

            if (keystatus == Statuses.Given)
            {
                return RedirectToAction("AllOrders");
            }

            var order = await _orderRepository.GetByIdAsync(orderId);
            await _orderRepository.RemoveAsync(order);

            return RedirectToAction("AllOrders");
        }
    }
}
