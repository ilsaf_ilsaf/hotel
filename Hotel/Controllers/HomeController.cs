﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Hotel.Data.Interface;
using Hotel.Entities;
using Hotel.Models;
using Hotel.Models.Home;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IRepository<Room> _repositoryRoom;
        private readonly IRepository<RoomCategory> _repositoryRoomCat;
        private readonly IMapper mapper;
        private readonly IRepository<RoomCategoryImages> _roomImages;

public HomeController(ILogger<HomeController> logger, IRepository<Room> repository,
            IMapper mapper, IRepository<RoomCategory> repositoryRoomCat, IRepository<RoomCategoryImages> roomImages)
        {
            _logger = logger;
            _repositoryRoom = repository;
            this.mapper = mapper;
            _repositoryRoomCat = repositoryRoomCat;
            _roomImages = roomImages;
        }

        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> AllRooms()
        {
            var rooms = await _repositoryRoomCat.GetAll()
                .ProjectTo<HomeListViewModel>(mapper.ConfigurationProvider)
                .OrderBy(x => x.Id)
                .ThenBy(x => x.Name)
                .ToListAsync();

            return View(rooms);
        }

        public async Task<IActionResult> AvaliableRooms(int id)
        {
            var allrooms = await _repositoryRoom.GetAll()
                .Where(x => x.RoomCategory.Id == id)
                .Include(x => x.RoomCategory)
                .ProjectTo<RoomListViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();

            return View(allrooms);
        }

        public async Task<IActionResult> RoomCategoryInfo(int id)
        {
            var room = await _repositoryRoomCat.GetAll()
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();  //.ProjectTo<HomeListViewModel>(mapper.ConfigurationProvider)

            var roomImages = await _roomImages.GetAll()
                .Where(x => x.RoomId == id)
                .ToListAsync();

            var model = new HomeListViewModel
            {
                Description = room.Description,
                Name = room.Name,
                PlacesQuantity = room.PlacesQuantity,
                Price = room.Price,
                RoomQuantity = room.RoomQuantity,
                RoomImages = roomImages
            };
            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
