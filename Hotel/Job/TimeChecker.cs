﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Hotel.Data.Interface;
using Hotel.Entities;
using Microsoft.EntityFrameworkCore;
using Quartz;

namespace Hotel.Job
{
    public class TimeChecker : IJob
    {
        private readonly IRepository<Order> _repositoryOrder;

        public TimeChecker(IRepository<Order> repositoryOrder)
        {
            _repositoryOrder = repositoryOrder;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            var orders = await _repositoryOrder.GetAll().ToListAsync();
            var time = new TimeSpan(24, 0, 0);
            foreach (var order in orders)
            {
                if (DateTime.Now.Subtract(order.OrderTime) > time && order.Statuses == Statuses.NotGiven)
                {
                    order.Statuses = Statuses.Expired;
                    order.OrderTime = DateTime.MinValue;
                    await _repositoryOrder.UpdateAsync(order);
                }
            }
        }
    }
}
